﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FileReaderUtility._Default" ValidateRequest="False" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2>File Reader Utility</h2>
    <div class="jumbotron">
        <p class="lead">Please select a text file and click on the read button.</p>


        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server">
            <ContentTemplate>
                <p>
                    <asp:FileUpload ID="FileUp" runat="server" accept=".txt, .xml" />
                </p>
                <p>
                    <asp:Button ID="ReadBtn" runat="server" Text="Read Selected File" CssClass="btn btn-primary" OnClick="ReadBtn_Click" />
                </p>
                <br />

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="ReadBtn" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div class="container">
        <div class="form-group">
            <h2>Result</h2>
            <textarea class="form-control" rows="8" placeholder="No file is loaded for reading!" id="TextResult" runat="server" readonly="readonly"></textarea>
        </div>
    </div>

</asp:Content>
