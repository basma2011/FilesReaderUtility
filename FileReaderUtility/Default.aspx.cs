﻿using System;
using System.IO;
using System.Web.UI;

namespace FileReaderUtility
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (TextResult.InnerText.Length > 0)
                    TextResult.InnerText.Remove(0);

        }

        protected void ReadBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (TextResult.InnerText.Length > 0)
                    TextResult.InnerText.Remove(0);

                ReadTxtFile();
            }
            catch (Exception ex)
            {
                Response.Write("An error occurred while reading the file : ");
                Response.Write(ex.Message);
            }
        }

        public bool ReadTxtFile()
        {
            try
            {
                if (FileUp != null && FileUp.HasFile)
                {
                    Stream stream = FileUp.FileContent;
                    TextReader l_reader = new StreamReader(stream);
                    string l_result = l_reader.ReadToEnd();
                    l_reader.Close();
                    TextResult.InnerText = l_result;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;

            }
        }
    }
}